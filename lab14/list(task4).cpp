#include <iostream>
#include <string>
#include <Windows.h>
#include <ctime>
using namespace std;

struct list
{
	list* next;
	int data;
};

list* createList();
void output(list* newList);
void task(list* newList);

int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	srand(time(0));

	list* newList = NULL;
	string menu;
	bool end = 0;
	while (!end)
	{
		cout << "\n1 - ������� ������\n2 - ����� ������������� ������ �� �����\n3 - �������� ������ �������(������ �� ����������)\n4 - �����\n����: ";
		cin >> menu;
		if (menu == "1")
		{
			if (!newList)
			{
				newList = createList();
			}
			else cout << "������ ��� ������" << endl;
		}
		else if (menu == "2")
		{
			cout << "������: " << endl;
			output(newList);
		}
		else if (menu == "3")
		{
			task(newList);
		}
		else if (menu == "4")
		{
			end = 1;
		}
		else cout << "������������ ����, ���������� �����" << endl;
	}

	return 0;
}

void add_element(list* newList, int data)
{//���������� � �����
	while (newList->next != NULL)
	{
		newList = newList->next;
	}
	list* temp = new list;
	temp->data = data;
	newList->next = temp;
	temp->next = NULL;
}
list* createList()
{
	cout << "������: " << endl;
	int sizeOfList;
	sizeOfList = rand() % 10;
	list* newList = new list;
	newList->next = NULL;
	newList->data = rand() % 10;
	int value;
	for (int i = 0; i < sizeOfList; i++)
	{
		value = rand() % 10;
		add_element(newList, value);
	}
	output(newList);
	return newList;
}
void output(list* newList)
{
	if (!newList)
	{
		return;
	}
	cout << newList->data << endl;
	output(newList->next);
}
void task(list* newList)
{
	if (newList == NULL)
	{
		cout << "������ ����" << endl;
		return;
	}
	int value = newList->data;
	add_element(newList, value);
	output(newList);
}