#include <iostream>
#include <string>
#include <Windows.h>
#include <ctime>
using namespace std;

struct list
{
	list* next;
	int data;
};

list* createList();
void output(list * newList);
list* task(list* FirstList, list* SecondList);

int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	srand(time(0));

	list* FirstList = createList();
	list* SecondList = createList();

	cout << "������ ������:" << endl;
	output(FirstList);

	cout << "\n������ ������:" << endl;
	output(SecondList);

	list* result;
	result = task(FirstList, SecondList);
	if (result == NULL) cout << "\n��� ���������� ���������" << endl;
	else
	{
		cout << "\n���������: " << endl;
		output(result);
	}
	return 0;
}

void add_element(list* newList, int data)
{//���������� � �����
	while (newList->next != NULL)
	{
		newList = newList->next;
	}
	list* temp = new list;
	temp->data = data;
	newList->next = temp;
	temp->next = NULL;
}
list* createList()
{
	int sizeOfList;
	sizeOfList = rand() % 10;
	list* newList = new list;
	newList->next = NULL;
	newList->data = rand() % 10;
	int value;
	for (int i = 0; i < sizeOfList; i++)
	{
		value = rand() % 10;
		add_element(newList, value);
	}
	return newList;
}
void output(list* newList)
{
	if (!newList)
	{
		return;
	}
	cout << newList->data << endl;
	output(newList->next);
}
bool findElement(list* LIST, int value)
{
	while (LIST != NULL)
	{
		if (LIST->data == value) return 1;
		else LIST = LIST->next;
	}
	return 0;
}

list* task(list* FirstList, list* SecondList)
{
	list* result = new list;
	result->next = NULL;
	bool noSameElements = 1;
	while (FirstList != NULL)
	{
		if (findElement(SecondList, FirstList->data) && !findElement(result, FirstList->data))
		{
			add_element(result, FirstList->data);
			noSameElements = 0;
		}
		FirstList = FirstList->next;
	}
	result = result->next;
	if (!noSameElements)
	{
		
		return result;
	}
	else return NULL;
}