#include <iostream>
#include <string>
#include <Windows.h>
using namespace std;

struct list
{
	list* next;
	string data;
};

list* createList();
void output(list *strList);
void only_vowels(list* strList);

int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);

	list* strList = NULL;
	string menu;
	bool end = 0;
	while (!end)
	{
		cout << "\n1 - ������� ������\n2 - ����� ������������� ������ �� �����\n3 - �������� ������ �������(������ �� ����������)\n4 - �����\n����: ";
		cin >> menu;
		if (menu == "1")
		{
			if (!strList)
			{
				strList = createList();
			}
			else cout << "������ ��� ������" << endl;
		}
		else if (menu == "2")
		{
			output(strList);
		}
		else if (menu == "3")
		{
			only_vowels(strList);
		}
		else if (menu == "4")
		{
			end = 1;
		}
		else cout << "������������ ����, ���������� �����" << endl;
	}

	return 0;
}
void add_element(list* strList, string data)
{//���������� � �����
	while (strList->next != NULL)
	{
		strList = strList->next;
	}
	list* temp = new list;
	temp->data = data;
	strList->next = temp;
	temp->next = NULL;
}
list* createList()
{
	cout << "������� ����� ��������� � ������?\n����: ";
	int sizeOfList;
	cin >> sizeOfList;
	list* strList = new list;
	strList->next = NULL;
	string str;
	cout << "������� ������" << endl;
	cin.ignore();
	for (int i = 0; i < sizeOfList; i++)
	{
		cout << i + 1 << "-� ������: ";
		getline(cin, str);
		add_element(strList, str);
	}
	return strList;
}
void output(list* strList)
{
	if (!strList)
	{
		return;
	}
	cout << strList->data << endl;
	output(strList->next);
}
bool is_vowel(char c)
{
	string en_vowels = "EYUIOA";
	for (int i = 0; i < en_vowels.size(); i++)
	{
		if (c == en_vowels[i] || toupper(c) == en_vowels[i]) return 1;
	}
	return 0;
}
void only_vowels(list* strList)
{
	while (strList != NULL)
	{
		for (int i = 0; i < strList->data.size(); )
		{
			if (!is_vowel(strList->data[i]))
			{
				strList->data.erase(strList->data.begin() + i);
			}
			else i++;
		}
		cout << strList->data << endl;
		strList = strList->next;
	}
}