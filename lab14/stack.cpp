#include <iostream>
#include <string>
#include <fstream>
#include <vector>
using namespace std;

//����� ��������� ����� ��������� �� ��.��������� ����, ��������� ������������ �������� ����������� ������(begin - end) � ���� ���������.
struct stack
{
	char info;
	stack* next;
};

void push(stack *top, char info);
char pop(stack* top);
bool addToStack(string path, stack* top);
bool CodeIsOk(stack* top);
void task(stack* top);
void TASK(stack* top, string file);

int main()
{
	setlocale(0, "");
	stack* top = new stack;
	top->next = NULL;

	string file = "cpp_code.txt";
	
	TASK(top, file);

	return 0;
}

char pop(stack* top)
{
	if (!top->next) return top->info;
	char c = top->info;
	top->info = top->next->info;
	top->next = top->next->next;
	return c;
}
void push(stack* top, char info)
{
	stack* temp = new stack;
	temp->info = top->info;
	temp->next = top->next;
	top->next = temp;
	top->info = info;
}
bool addToStack(string path, stack* top)
{
	char temp;
	ifstream file(path);
	if (!file.is_open())
	{
		cout << "������ �������� �����!" << endl;
		return 0;
	}
	string buffer;
	while (file.get(temp))
	{
		if (temp == '}' || temp == '{')
		{
			buffer.push_back(temp);
		}
	}
	if (buffer.size() == 0)
	{
		cout << "��� ����������� ������ � ��������� ��� ���� ����!" << endl;
		return 0;
	}
	for (int i = 0; i < buffer.size(); i++)
	{
		if (buffer[i] == '}' || buffer[i] == '{')
		{
			push(top, buffer[i]);
		}
	}
	cout << endl;
	file.close();
	return 1;
}
bool CodeIsOk(stack* top)
{
	vector <bool> mask;
	char c;
	while (top->next)
	{
		c = pop(top);
		mask.push_back(c == '}');
	}
	if (mask.size() % 2 != 0 || mask.size() == 0)
	{
		return 0;
	}
	int size = mask.size();
	for (int i = 0; i < mask.size(); )
	{
		if (!mask[i]) continue;
		for (int j = mask.size() - 1; j > i; )
		{
			if (!mask[j])
			{
				mask.erase(mask.begin() + j);
				break;
			}
			else
			{
				j--;
			}
		}
		if (mask.size() == size / 2) break;
	}
	for (int i = 0; i < mask.size(); )
	{
		if (mask[i]) mask.erase(mask.begin() + i);
		else i++;
	}
	if (mask.size() == 0) return 1;
	else return 0;
}
void task(stack* top)
{
	cout << "��������� ������" << endl;
	cout << "." << endl;
	cout << ".." << endl;
	cout << "..." << endl;
	if (CodeIsOk(top)) cout << "c����� ����������� ���������\n" << endl;
	else cout << "������ ����������� �����������\n" << endl;
}
void TASK(stack* top, string file)
{
	string changePath;
	bool end = 0;
	while (!end)
	{
		cout << "���� �� ���������: \"\ cpp_code.txt \"\\n������� ��������?(0 - ���, 1 - ��)\n����: ";
		cin >> changePath;
		if (changePath == "1")
		{
			cout << "������� ��� ������ �����: ";
			string newFile;
			cin >> newFile;
			ofstream file(newFile);
			file.close();
			cout << "���� ������" << endl;
			end = 1;
		}
		else if (changePath == "0")
		{
			end = 1;
		}
	}
	if (addToStack(file, top)) task(top);
}