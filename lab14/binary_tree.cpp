#include <iostream>
#include <string>
#include <vector>
#include <Windows.h>
using namespace std;

struct tree
{
	string info;
	tree* right;
	tree* left;
};

tree* root = NULL;
void create();
void output(tree* root, int l);
void task(tree* root);

int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	string choice;
	bool exit = 0;
	while (!exit)
	{
		cout << "1 - ������� ������\n2 - ����� �� �����\n3 - ������ � �������(�� ��������)\n4 - �����\n����: ";
		cin >> choice;
		cin.ignore();
		if (choice == "1")
		{
			create();
		}
		else if (choice == "2")
		{
			cout << "\n\n";
			output(root, 0);
			cout << "\n\n";
		}
		else if (choice == "3")
		{
			task(root);
		}
		else if (choice == "4")
		{
			cout << "\n�����" << endl;
			exit = 1;
		}
		else
		{
			cout << "\n������������ ����!" << endl;
		}
	}


	return 0;
}
tree* createTree(tree* root, tree* r, string info)
{
	if (!r)
	{
		r = new tree;
		if (!r)
		{
			cout << "�� ������� ������" << endl;
			exit(0);
		}
		r->left = NULL;
		r->right = NULL;
		r->info = info;
		if (!root) return r;
		if (info < root->info) root->left = r;
		else root->right = r;
		return r;
	}
	if (info < r->info) createTree(r, r->left, info);
	else createTree(r, r->right, info);
	return root;
}
void create()
{
	cout << "������� ����� � ������: ";
	int count;
	cin >> count;
	string temp;
	cin.ignore();
	for (int i = 0; i < count; i++)
	{
		cout << "������� " << i + 1 << "-� �������: ";
		getline(cin, temp);
		root = createTree(root, root, temp);
	}
}
void output(tree* root, int l)
{
	if (!root) return;

	output(root->right, l + 1);
	for (int i = 0; i < l; i++) cout << " ";
	cout << root->info << endl;
	output(root->left, l + 1);
}
tree* search_tree(tree* root, string key)
{
	if (!root) return root;
	while (root->info != key) {
		if (key < root->info) root = root->left;
		else root = root->right;
		if (root == NULL) break;
	}
	return root;
}
bool find_symbol(string str, char c)
{
	int size = str.size();
	bool result = 0;
	for (int i = 0; i < size; i++)
	{
		if (str[i] == c)
		{
			result = 1;
			break;
		}
	}
	return result;
}
void approve_symbol(tree* root, char c, vector<bool>& mask)
{
	if (!root) return;

	approve_symbol(root->right, c, mask);
	mask.push_back(find_symbol(root->info, c));
	approve_symbol(root->left, c, mask);
}
bool repeated_symbols(string str, char c, int j)
{
	bool result = 0;
	for (int i = 0; i < j; i++)
	{
		if (str[i] == c)
		{
			result = 1;
			break;
		}
	}
	return result;
}
void task(tree* root)
{
	cout << "**������� ������, �������������� �� ���� ��������, ������������� � ������ ������ ������**\n���������: ";
	vector<bool> mask;
	int count;
	for (int i = 0; i < root->info.size(); i++)
	{
		if (!repeated_symbols(root->info, root->info[i], i))
		{
			count = 0;
			mask.clear();
			approve_symbol(root, root->info[i], mask);
			int size = mask.size();

			for (int j = 0; j < size; j++)
			{
				if (mask[j]) count++;
			}
			if (count == size) cout << root->info[i];
		}
	}
	cout << endl;
}