#include <iostream>
using namespace std;

double RootK(double X, int K, int N);

int main()
{
	double X;
	int K, N;
	cout << "X = ";
	cin >> X;
	cout << "K = ";
	cin >> K;
	for (N = 1; N < 7; N++)
		cout << "N = " << N << " Result: " << RootK(X, K, N) << endl;

	return 0;
}

double RootK(double X, int K, int N) 
{
	if (N == 0)
		return 1;
	else
	{
		N--;
		return RootK(X, K, N) - (RootK(X, K, N) - X/(RootK(X, K, N) * (K - 1)) ) / K;
	}

}