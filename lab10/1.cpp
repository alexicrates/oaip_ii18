#include <iostream>
using namespace std;

void input(int* A, int N);
void RemoveX(int* A, int* N, int X);
void output(int* A, int N);

int main() {
	int NA, NB, NC;
	int X;

	cout << "Size of A: ";
	cin >> NA;
	int* A = new int[NA];
	cout << "Enter A: ";
	input(A, NA);
	cout << "Delete number ";
	cin >> X;
	RemoveX(A, &NA, X);
	output(A, NA);
	cout << "Size of A: " << NA << endl;

	cout << "\nSize of B: ";
	cin >> NB;
	int* B = new int[NB];
	cout << "Enter B: ";
	input(B, NB);
	cout << "Delete number ";
	cin >> X;
	RemoveX(B, &NB, X);
	output(B, NB);
	cout << "Size of B: " << NB << endl;

	cout << "\nSize of C: ";
	cin >> NC;
	int* C = new int[NC];
	cout << "���� C: ";
	input(C, NC);
	cout << "Delete number ";
	cin >> X;
	RemoveX(C, &NC, X);
	output(C, NC);
	cout << "\nSize of C: " << NC << endl;

	delete[] A, B, C;
	return 0;
}

void RemoveX(int* A, int* N, int X) {

	for (int i = 0; i < *N; i++)
	{
		if (A[i] == X)
		{
			for (int j = i; j < *N; ++j)
				A[j] = A[j + 1];
			--* N;
		}
	}
	for (int i = 0; i < *N; i++)
	{
		if (A[i] == X)
			RemoveX(A, N, X);
	}
}
void input(int* A, int N)
{
	for (int i = 0; i < N; i++)
		cin >> A[i];
}
void output(int* A, int N)
{
	for (int i = 0; i < N; i++)
		cout << A[i] << " ";
	cout << endl;
}