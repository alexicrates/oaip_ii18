#include <iostream>
#include <Windows.h>
using namespace std;

unsigned int PosSub(char* S0, char* S, int K, int N);

int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);

	char S[30], S0[30];
	cout << "String S: ";
	gets_s(S);
	cout << "String S0: ";
	gets_s(S0);
	unsigned int K, N;
	for (int i = 0; i < 3; i++)
	{
		cout << "Symbol K" << i + 1 << ":";
		cin >> K;
		cout << "Symbol N" << i + 1 << ":";
		cin >> N;
		cout << "Result: " << PosSub(S0, S, K, N) << endl;
	}
	return 0;
}

unsigned int PosSub(char* S0, char* S, int K, int N) {
	K--;
	int position;
	if (K > strlen(S) || K < 0)
		return 0;
	if (K + N > strlen(S))
		N = strlen(S);
	char* ptr = S + K;
	char* place = strstr(ptr, S0);
	bool success = 0;
	for (int i = K; i < K + N; i++)
	{
		if (&S[i] == place)
		{
			success = 1;
			position = i + 1;
			break;
		}
	}
	if (success) return position;
	else return 0;
}