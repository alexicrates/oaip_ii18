#include <iostream>
#include <Windows.h>
using namespace std;
struct buyer            //��������� "����������"
{
	char surname[100];
	char name[100];
	char patronymic[100];
	bool check_phone : 1;    //��� ��������(1-��������, 0 - ���������)
	union phone {           //����������� ������ ��������/��������� ����� ��������
		char home_num[7];
		char mob_num[14];
	}phone_;

	char addres[100];         //�����
	char card_num[17];         //�������� �����
};

int main() 
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);

	const int size = 11;
	buyer arr[size];
	bool  phone;
	char phone_num[50];
	bool search = false;
	int Input = 0;
	int _size = 0;
	int check = 0;
	enum menu { input = 1, output, sort, srch, change, del, exit };
	while (check != 7)
	{
		cout << "1)���� ������� �������� \n" << "2)����� �� ����� ������� ��������\n"
			<< "3)���������� ������� ��������\n" << "4)����� � ������� �������� �� ��������� ���������\n"
			<< "5)��������� �������� ���������\n" << "6)�������� ��������� �� �������\n" << "7)�����\n"
			<< "___________________________________________________________________________________" << endl;
		cout << "����: ";
		cin >> check;
		switch (check)
		{
		case input:
			cout << "����� ������� ��� " << size - _size - 1 << " �������" << endl;
			cout << "������� ������� �������: ";
			cin >> Input;
			if (Input > (size - _size - 1) || Input <= 0)
				cout << "---������---" << endl;
			else
			{
				cout << "������� ������:" << endl;
				for (int i = _size; i < Input + _size; i++)
				{
					cout << i + 1 << "-� ������:\n" << "���: ";
					cin >> arr[i].surname >> arr[i].name >> arr[i].patronymic;
					cout << "��������(1) ��� ���������(0) �������? ";
					cin >> phone;
					cout << "����� ��������: ";
					arr[i].check_phone = phone;
					cin >> phone_num;
					if (arr[i].check_phone) {
						strncpy_s(arr[i].phone_.home_num, phone_num, 6);

					}
					else {
						strncpy_s(arr[i].phone_.mob_num, phone_num, 13);
					}
					cout << "�����(��� ��������): ";
					cin >> arr[i].addres;
					cout << "����� ��������� �����: ";
					cin >> arr[i].card_num;
				}
				_size += Input;
			}
			cout << "___________________________________________________________________________________" << endl;
			break;
		case output:
			for (int i = 0; i < _size; i++)
			{
				cout << endl << i + 1 << "-� ������:\t ���: ";
				cout << arr[i].surname << " " << arr[i].name << " " << arr[i].patronymic << " ";
				cout << "| ����� ��������: ";
				if (arr[i].check_phone == 1)
					cout << arr[i].phone_.home_num << " ";
				else cout << arr[i].phone_.mob_num << " ";
				cout << "| �����: " << arr[i].addres << "| ����� ��������� �����: " << arr[i].card_num;

			}

			cout << "\n___________________________________________________________________________________" << endl;

			break;
		case sort:
			buyer A;
			for (int i = 0; i < _size; i++)
			{
				for (int j = i; j < _size; j++)
				{
					if (strcmp(arr[i].surname, arr[j].surname) > 0)
					{
						A = arr[i];
						arr[i] = arr[j];
						arr[j] = A;
						break;
					}
				}
			}
			cout << "���������� ������ �������\n___________________________________________________________________________________" << endl;
			break;
		case srch:
			cout << "�������� �������� ������: " << endl << "1)�������; 2)���; 3)��������; 4) �������� �����; "
				<< "5)��� ��������; 6)����� ��������; 7)����� ��������� �����." << endl;
			search = false;
			char str_search[100];
			int search_option;
			cin >> search_option;
			switch (search_option) {
			case 1:
				cout << "������� �������:";
				cin >> str_search;
				for (int i = 0; i < _size; i++)
				{
					if (strcmp(arr[i].surname, str_search) == 0)
					{
						search = true;
						cout << i + 1 << "-� ����" << endl;
					}

				}
				break;
			case 2:
				cout << "������� ���: ";
				cin >> str_search;
				for (int i = 0; i < _size; i++)
				{
					if (strcmp(arr[i].name, str_search) == 0)
					{
						search = true;
						cout << i + 1 << "-� ����" << endl;
					}

				}
				break;
			case 3:
				cout << "������� ��������: ";
				cin >> str_search;
				for (int i = 0; i < _size; i++)
				{
					if (strcmp(arr[i].patronymic, str_search) == 0)
					{
						search = true;
						cout << i + 1 << "-� ����" << endl;
					}

				}
				break;
			case 4:
				cout << "������� �����: ";
				cin >> str_search;
				for (int i = 0; i < _size; i++)
				{
					if (strcmp(arr[i].addres, str_search) == 0)
					{
						search = true;
						cout << i + 1 << "-� ����" << endl;
					}

				}
				break;
			case 5:
				cout << "����� ��� ��������(�������� - 1, ��������� - 0): ";
				cin >> phone;
				for (int i = 0; i < _size; i++)
				{
					if (arr[i].check_phone == phone)
					{
						cout << i + 1 << "-� ������" << endl;
						search = true;
					}
				}
				break;
			case 6:
				cout << "������� ����� ��������: ";
				cin >> str_search;
				for (int i = 0; i < _size; i++)
				{
					if (strcmp(arr[i].phone_.home_num, str_search) == 0 || strcmp(arr[i].phone_.mob_num, str_search) == 0)
					{
						cout << i + 1 << "-� ������";
						search = true;
					}
				}
				break;
			case 7:
				cout << "������� ����� ��������� �����: ";
				cin >> str_search;
				for (int i = 0; i < _size; i++)
				{
					if (strcmp(arr[i].card_num, str_search) == 0)
					{
						search = true;
						cout << i + 1 << "-� ����" << endl;
					}
				}
				break;

			default: cout << "---������---" << endl;

			}
			if (!search) cout << "�� �������" << endl;
			cout << "\n___________________________________________________________________________________" << endl;
			break;
		case change:
			int search1, search2;
			cout << "������� ����� ������: ";
			cin >> search1;
			search1--;
			if (search1 < 0 || search1 >= _size)  cout << "������ �� �������";
			else {
				cout << "���������� ��������( 1)�������, 2)���, 3)��������, 4)�������� �����, 5)��� ��������, 6)����� ��������, 7)����� ��������� �����): ";
				cin >> search2;

				cout << "������� ����� ������: ";
				switch (search2) {
				case 1:
					cin >> arr[search1].surname;
					break;
				case 2:
					cin >> arr[search1].name;
					break;
				case 3:
					cin >> arr[search1].patronymic;
					break;
				case 4:
					cin >> arr[search1].addres;
					break;
				case 5:
					cin >> phone;
					arr[search1].check_phone = phone;
					break;
				case 6:
					cin >> phone_num;
					if (arr[search1].check_phone == 1) {
						strncpy_s(arr[search1].phone_.mob_num, phone_num, 6);
					}
					else strncpy_s(arr[search1].phone_.mob_num, phone_num, 13);
					break;
				case 7:
					cin >> arr[search1].card_num;
					break;
				default:
					cout << "---������---" << endl;
				}
			}
			cout << "\n___________________________________________________________________________________" << endl;
			break;
		case del:
			int a;
			cout << "����� ������ �������?" << endl;
			cin >> a;
			a--;
			if (a < _size) {
				for (int i = a; i < size; i++)
					arr[i] = arr[i + 1];
				_size--;
				cout << "������ �������" << endl;
			}
			else cout << "������ �� �������";
			cout << "\n___________________________________________________________________________________" << endl;
			break;
		case exit:

			cout << "�����...";
			break;

		default:
			check = exit;
			cout << "---������---" << endl;
		}
	}
	return 0;
}
