#include <iostream>
#include <string>
#include <fstream>
#include <ctime>
#include <Windows.h>
using namespace std;

void task2();
void task3();
void task4();
void task5();

int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	srand(time(0));
	void (*task[])() = {task2, task3, task4, task5};
	int check_menu;
	do
	{
		cout << "\n������ ������� - 1 \n������ ������� - 2 \n�������� ������� - 3 \n����� ������� - 4 \n����� - 0\n����: ";
		cin >> check_menu;
		if (check_menu >= 1 && check_menu <= 4) (*task[check_menu - 1])();
		else if(check_menu == 0) cout << "�����..." << endl;
		else cout << "Error" << endl;

	} while (check_menu != 0);
	return 0;
}
void counting_sort_mm(int* array, int n, int min, int max)
{
	int i, j, z;

	int range = max - min + 1;
	int* count = (int*)malloc(range * sizeof(*array));

	for (i = 0; i < range; i++) count[i] = 0;
	for (i = 0; i < n; i++) count[array[i] - min]++;

	for (i = min, z = 0; i <= max; i++) {
		for (j = 0; j < count[i - min]; j++) {
			array[z++] = i;
		}
	}

	free(count);
}
void task2()
{
	cout << "������� 2:\n�������� ���������� ����� � �������� ��������... " << endl;
	ofstream file;
	file.open("task2.txt");
	int temp;
	for (int i = 0; i < 5; i++)
	{
		for (int j = 0; j < 5; j++)
		{
			temp = rand() % 100;
			file << temp << " ";
			cout << temp << " ";
		}
		cout << endl;
		file << endl;
	}
	file.close();
	cout << "������� ��������� � ����� task2.txt" << endl;
	cout << "�������� ����������...\n";
	fstream fs;
	fs.open("task2.txt", fstream::in | fstream::out | fstream::app);
	int arr[25];
	for (int i = 0; i < 25; i++)
	{
		fs >> arr[i];
	}
	counting_sort_mm(arr, 25, 0, 100);
	fs << "\n Result:\n";
	for (int i = 0, k = 0; i < 5; i++)
	{
		for (int j = 0; j < 5; j++, k++)
		{
			cout << arr[k] << " ";
			fs << arr[k] << " ";
		}
		cout << endl;
		fs << endl;
	}
	fs.close();
	cout << "\n��������������� �������� ������� ��������� � ����� �����" << endl;
}
void task3()
{
	cout << "������� 3:" << endl;
	string text1, text2;
	cout << "���� � ������� �����(�� ��������� text1.txt): ";
	cin >> text1;
	cout << "���� �� ������� �����(�� ��������� text2.txt): ";
	cin >> text2;
	char* txt1 = new char[300];
	char* txt2 = new char[300];

	int i1 = 0, i2 = 0;
	bool txt1_is_open, txt2_is_open;
	ifstream ifs;
	ifs.open(text1);
	if (!ifs.is_open())
	{
		cout << "������ �������� ������� �����!!!" << endl;
	}
	else
	{
		while (ifs.get(txt1[i1]))
		{
			i1++;
		}
	}
	txt1_is_open = ifs.is_open();
	ifs.close();
	ifs.open(text2);
	if (!ifs.is_open())
	{
		cout << "������ �������� ������� �����!!!" << endl;
	}
	else
	{
		while (ifs.get(txt2[i2]))
		{
			i2++;
		}
	}
	txt2_is_open = ifs.is_open();
	ifs.close();
	if (txt1_is_open && txt2_is_open)
	{
		ofstream ofs;
		ofs.open(text1);
		for (int i = 0; i < i2; i++)
		{
			ofs << txt2[i];
		}
		for (int i = 0; i < i1; i++)
		{
			ofs << txt1[i];
		}
		ofs.close();
		cout << "���������" << endl;
	}
	else cout << "���������� ��� ���!" << endl;
		delete[] txt1;
		delete[] txt2;
}
void task4()
{
	cout << "������� 4: " << endl;
	char delete_this_symbols[] = { " ,.;:!?\n\b\t�" };
	string path;
	unsigned int K;
	cout << "������� ��� ��������� ��������� �����(�� ��������� task4.txt): ";
	cin >> path;
	
	ifstream file;
	file.open(path);
	if (!file.is_open())
	{
		cout << "������ �������� �����" << endl;
	}
	else
	{
		cout << "������� K: ";
		cin >> K;
		char text[300];
		int size = 0;
		while (file.get(text[size]))
		{
			cout << text[size];
			size++;
		}
		for (int i = 0; i < size; i++)
		{
			for (int j = 0; j < strlen(delete_this_symbols); j++)
			{
				if (text[i] == delete_this_symbols[j])
				{
					text[i] = ' ';
					break;
				}
			}
		}
		file.close();
		ofstream ofs;
		ofs.open("result.txt");
		char* ptr = text;
		char* place;
		int k = 0;
		for (int i = 0; i < size; i++)
		{
			if (text[i] == ' ') k++;
		}
		int m = 0;
		//�������� � �������� ���������
		for (int i = 0; i < k; i++)
		{
			place = strchr(ptr, ' ');
			if (place - ptr == K)
			{
				for (int j = 0; j < K; j++) ofs << ptr[j];
				ofs << " ";
			}
			ptr = place + 1;
		}
		/////////////////////////////////
		file.close();
		cout << "\n���������. ��������� �������� � result.txt" << endl;
	}
}
void task5()
{
	string path = "task5.txt";
	cout << "������� ��� ����� � �������(�� ��������� task5.txt): ";
	cin >> path;
	ifstream file(path);
	float temp;
	float count = 0, sum = 0;
	while (!file.eof())
	{
		file >> temp;
		if (temp == int(temp))
		{
			count++;
			sum += temp;
		}
	}
	cout << "���������� ����� �����: " << count << "\n����� ���� ����� �����: " << sum << endl;
	file.close();
}

