#include <iostream>
#include <fstream>
#include <ctime>
using namespace std;


void createMatrix(int n, int k, string path);
void addMatrix(int k, string path);
void output(int n, int k, string path);

int main()
{
	setlocale(0, "");
	srand(time(0));
	string file1 = "File1.txt", file2 = "File2.txt";
	int n1, n2, k;
	cout << "������� ������ �������� � " << file1 << "(n1): ";
	cin >> n1;
	if (n1 < 0)
	{
		cout << "������! ����������� ����! " << endl;
		return 0;
	}
	cout << "������� ������ �������� � " << file2 << "(n2): ";
	cin >> n2;
	if (n2 < 0)
	{
		cout << "������! ����������� ����! " << endl;
		return 0;
	}
	cout << "������� ����� �������� � ����� � ������ �� ������ (k): ";
	cin >> k;
	while (k <= 0)
	{
		cout << "����������� ����! ���������� �����: " << endl;
		cin >> k;
	}
	cout << file1 << ": " << endl;
	createMatrix(n1, k, file1);
	output(n1, k, file1);
	cout << file2 << ": " << endl;
	createMatrix(n2, k, file2);
	output(n2, k, file2);
	if (n1 == n2) cout << "n1 = n2\n������ �� ������" << endl;
	else if (n1 == 1) cout << "n1 = 1\n������ �� ������" << endl;
	else if (n1 < n2)
	{
		cout << "n1 < n2" << endl;
		for (int i = 0; i < n2 - n1; i++)	addMatrix(k, file1);
		n1 += (n2 - n1);
	}
	else if (n1 > n2)
	{
		cout << "n1 > n2" << endl;
		for (int i = 0; i < n1 - n2; i++) addMatrix(k, file2);
		n2 += (n1 - n2);
	}
	cout << file1 << ": " << endl;
	output(n1, k, file1);
	cout << file2 << ": " << endl;
	output(n2, k, file2);
	return 0;
}

void createMatrix(int n, int k, string path)
{
	float** arr = new float* [k];
	for (int i = 0; i < k; i++)
	{
		arr[i] = new float[k];
	}
	float value;
	ofstream file(path, ios::binary);
	for (int l = 0; l < n; l++)
	{
		for (int i = 0; i < k; i++)
		{
			for (int j = 0; j < k; j++)
			{
				value = (rand() & 10000) * 0.01;
				file.write((char*)& value, sizeof(value));
			}
		}
	}
	file.close();
}
void addMatrix(int k, string path)
{
	float one = 1;
	ofstream file(path, ios::app | ios::binary);
	for (int i = 0; i < k; i++)
	{
		for (int j = 0; j < k; j++)
		{
			file.write((char*)& one, sizeof(float));
		}
	}
	file.close();
}
void output(int n, int k, string path)
{
	ifstream file(path);
	if (!file.is_open()) cout << "���� �� ������" << endl;
	else
	{
		float temp;
		for (int l = 0; l < n; l++)
		{
			for (int i = 0; i < k; i++)
			{
				for (int j = 0; j < k; j++)
				{
					file.read((char*)& temp, sizeof(temp));
					cout << temp << "\t";
				}
				cout << "\n\n";
			}
			cout << "\n";
		}
	}
	file.close();
}