#include <iostream>
#include <string>
#include <fstream>
#include <Windows.h>
using namespace std;

struct worker
{
	char name[50], surname[50], patronymic[50];
	int mailIndex[6];
	string country, region, district, town, street;
	int house, flat;
	string nationality;
	int birthDate[3];
	int numOfDepartment;
	int serviceNum[8];
	char education[50];
	int startWorkYear;
};

string createNewFile();
void addNewData(string path, int& size);
void output(string path, int size);
void deleteData(string path, int& size);
void sorting(string path, int size);

int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	int count = 0;
	string file;
	bool b = 1;
	int menu;
	while (b)
	{
		cout << "\n\t\t**����**" << endl;
		cout << "1 - �������� �����\n2 - �������� �������� � ����\n3 - ����� �� �����\n4 - ������� �������\n5 - ����������(���������)\n\n����: ";
		cin >> menu;
		switch (menu)
		{
		case 1:
			file = createNewFile();
			break;
		case 2:
			addNewData(file, count);
			break;
		case 3:
			output(file, count);
			break;
		case 4:
			deleteData(file, count);
			break;
		case 5:
			sorting(file, count);
			break;
		default:
			cout << "������!!!" << endl;
			b = 0;
			break;
		}
		if (b)
		{
			cout << "\n����������? (0 - �����, 1 - ����������): ";
			cin >> b;
		}
	}
	return 0;
}

string createNewFile()
{
	cout << "\n\t\t**�������� ������ �����**" << endl;
	cout << "������� ��� ������ �����: ";
	string path;
	cin >> path;
	ofstream newFile(path, ios::binary);
	if (newFile.is_open()) cout << "���� ������� ������" << endl;
	newFile.close();
	return path;
}
void addNewData(string path, int& size)
{
	ofstream file(path, ios::binary | ios::app);

	cout << "\t\t**���������� ��������� � ����� �����**" << endl;
	if (!file.is_open())
	{
		cout << "���� �� ������" << endl;
		return;
	}
	cout << "������� ���������� ����� ���������( 0 - ����� ): ";
	int count = 0;
	cin >> count;
	while (count < 0)
	{
		cout << "������������ ����! ���������� �����\n����: ";
		cin >> count;
	}
	if (count <= 0) return;
	worker temp;
	for (int i = 0; i < count; i++)
	{
		cout << size + i + 1 << "-� �������" << endl;
		cout << "���: ";
		cin >> temp.name;
		cout << "�������: ";
		cin >> temp.surname;
		cout << "��������: ";
		cin >> temp.patronymic;
		cout << "�����: " << endl;
		cout << "- �������� ������(6 ���� ����� ������): ";
		for (int j = 0; j < 6; j++) cin >> temp.mailIndex[j];
		cout << "- ������: ";
		cin >> temp.country;
		cout << "- �������: ";
		cin >> temp.region;
		cout << "- �����: ";
		cin >> temp.district;
		cout << "- �����: ";
		cin >> temp.town;
		cout << "- �����: ";
		cin >> temp.street;
		cout << "- ���: ";
		cin >> temp.house;
		cout << "- ��������: ";
		cin >> temp.flat;
		cout << "��������������: ";
		cin >> temp.nationality;
		cout << "���� ��������(����/�����/���): ";
		for (int j = 0; j < 3; j++) cin >> temp.birthDate[j];
		cout << "����� ����: ";
		cin >> temp.numOfDepartment;
		cout << "��������� �����(8 ���� ����� ������): ";
		for (int j = 0; j < 8; j++) cin >> temp.serviceNum[j];
		cout << "�����������(����� �������, ����� �������, ������� ����������� �����������, ������ ����������������): ";
		cin.ignore();
		cin.getline(temp.education, 50);
		cout << "��� ����������� �� ������: ";
		cin >> temp.startWorkYear;
		file.write((char*)& temp, sizeof(temp));
	}
	file.close();
	size += count;
}
void output(string path, int size)
{
	ifstream file(path);
	if (!file.is_open()) cout << "���� �� ������" << endl;
	else
	{
		cout << "\n\t\t**����� �� �����**" << endl;
		if (size == 0)
		{
			cout << "���� ����" << endl;
			return;
		}
		worker* temp = new worker[size];
		for (int i = 0; i < size; i++)
		{
			file.read((char*)& temp[i], sizeof(temp[i]));
			cout << i + 1 << "-�� �������" << endl;
			cout << "���: " << temp[i].surname << " " << temp[i].name << " " << temp[i].patronymic << "\n�����:\n - �������� ������: ";
			for (int j = 0; j < 6; j++) cout << temp[i].mailIndex[j];
			cout << "\n - ������: " << temp[i].country << "\n - �������: " << temp[i].region << "\n - �����: " << temp[i].district << "\n - �����: " << temp[i].town << "\n - �����: "
				<< temp[i].street << "\n - ���: " << temp[i].house << "\n - ��������: " << temp[i].flat << "\n��������������: " << temp[i].nationality << "\n���� ��������(����/�����/���): "
				<< temp[i].birthDate[0] << '/' << temp[i].birthDate[1] << '/' << temp[i].birthDate[2] << "\n����� ����: " << temp[i].numOfDepartment << "\n��������� �����: ";
			for (int j = 0; j < 8; j++) cout << temp[i].serviceNum[j];
			cout << "\n�����������: " << temp[i].education << "\n��� ����������� �� ������: " << temp[i].startWorkYear << endl;
			cout << "\n\n";
		}
	}
	file.close();
}
void deleteData(string path, int& size)
{
	cout << "\t\t**�������� ��������� �� �����**" << endl;
	if (size == 0)
	{
		cout << "���� ����" << endl;
		return;
	}
	ifstream file(path, ios::binary);
	if (file.is_open())
	{
		worker* arr = new worker[size];
		for (int i = 0; i < size; i++)
		{
			file.read((char*)& arr[i], sizeof(worker));
		}
		file.close();

		cout << "������� ����� ���������� ��������: ";
		int elementToDeleting;
		cin >> elementToDeleting;
		if (elementToDeleting <= 0 || elementToDeleting > size)
		{
			cout << "������! ������������ ����!" << endl;
			return;
		}
		elementToDeleting--;
		for (int i = elementToDeleting; i < size - 1; i++)
		{
			arr[i] = arr[i + 1];
		}
		size--;

		ofstream file_(path, ios::binary);
		for (int i = 0; i < size; i++)
		{
			file_.write((char*)& arr[i], sizeof(worker));
		}
		file_.close();
		cout << "��������� �������" << endl;
		if (size != 0)
		{
			cout << " ���������: " << endl;
			output(path, size);
		}
	}
	else
	{
		cout << "���� �� ������" << endl;
		file.close();
	}

}
void sorting(string path, int size)
{
	cout << "\t\t**����������**" << endl;
	if (size == 0)
	{
		cout << "���� ����" << endl;
		return;
	}
	worker* arr = new worker[size];
	ifstream fromFile(path, ios::binary);
	if (fromFile.is_open())
	{
		for (int i = 0; i < size; i++)
		{
			fromFile.read((char*)& arr[i], sizeof(worker));
		}
		fromFile.close();
		int SIZE = size;
		bool sorted = 0;
		while (!sorted)
		{
			sorted = 1;
			for (int i = 0; i < size - 1; i++)
			{
				if (strcmp(arr[i].surname, arr[i + 1].surname) > 0)
				{
					swap(arr[i], arr[i + 1]);
					sorted = 0;
				}
			}
			size--;
		}
		ofstream toFile(path, ios::binary);
		for (int i = 0; i < SIZE; i++)
		{
			toFile.write((char*)& arr[i], sizeof(worker));
		}
		toFile.close();
		cout << "���������� ������ �������!\n���������:" << endl;
		output(path, SIZE);
	}
	else
	{
		cout << "���� �� ������" << endl;
		fromFile.close();
	}
}